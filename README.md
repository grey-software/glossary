# Glossary

Welcome to Grey Software Glossary!

When you’re new to tech, it can feel like you’ve stumbled into a conversation where everyone knows what they’re talking about—except, of course, you.

Sometimes a short and simple explanation is all you need to get your head around a new concept.


