import theme from 'grey-docs'

export default theme({
    components: true,
    content: {
        liveEdit: false
    },
    docs: {
        primaryColor: '#64748b'
    },
    head: {
        title: 'Glossary by Grey Software | Democratizing Software Education!',
        meta: [
            {
                name: 'og:title',
                content: 'Glossary by Grey Software | Democratizing Software Education!'
            },
            {
                name: 'og:description',
                content: 'New to software? Our glossary allows you to learn the meanings of technical terms and explore them further!'
            },
            {
                name: 'og:image',
                content: '/preview.png'
            },
            {
                name: 'twitter:card',
                content: 'summary_large_image'
            },
            {
                name: 'twitter:title',
                content: 'Glossary by Grey Software | Democratizing Software Education!'
            },
            {
                name: 'twitter:description',
                content: 'New to software? Our glossary allows you to learn the meanings of technical terms and explore them further!'
            },
            {
                name: 'twitter:image',
                content: '/preview.png'
            },
        ],
        script: [
            {
                src: 'https://plausible.io/js/plausible.js',
                async: true,
                defer: true,
                'data-domain': 'glossary.grey.software',
            },
        ]
    }
})

