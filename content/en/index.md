---
title: Glossary  
description: ''
position: 1
category: Glossary
---

When you’re new to tech, it can feel like you’ve stumbled into a conversation
where everyone knows what they’re talking about—except, of course, you.

Sometimes a short and simple explanation is all you need to get your head around
a new concept.

### HTML

<search-anywhere query="What is HTML?"></search-anywhere>
<br> </br>

HyperText Markup Language is the document standard of the World Wide Web for
creating web pages and web applications.

Web browsers receive HTML documents from a web server or from local storage and
render the documents into multimedia web pages. HTML describes the structure of
a web page semantically.

_From [Glossary Tech](https://glossarytech.com/)_

### CSS

<search-anywhere query="What is CSS?"></search-anywhere>
<br> </br>
A stylesheet language that is used for presentation and formatting content on
the web-pages, including font, size, color, spacing, border and location of HTML
information.

_From [Glossary Tech](https://glossarytech.com/)_

### Javascript

<search-anywhere query="What is Javascript?"></search-anywhere>
<br> </br>
A client-side scripting language primarily used to make web pages interactive.
In other words, it's responsible for the 'behavior' of a website, i.e. how HTML
elements and CSS style animate and move around on the page.


### Node.js

<search-anywhere query="What is Node.js?"></search-anywhere>
<br> </br>
Node.js is an open-source, cross-platform, JavaScript runtime environment that
executes JavaScript code outside of a web browser.

Node.js lets developers use JavaScript to write command line tools and for
server-side scripting—running scripts server-side to produce dynamic web page
content before the page is sent to the user's web browser.

Consequently, Node.js represents a "JavaScript everywhere" paradigm, unifying
web-application development around a single programming language, rather than
different languages for server-side and client-side scripts.

### Vue.js

<search-anywhere query="What is Vue.js?"></search-anywhere>
<br> </br>

Vue.js is an open-source Model–View–Viewmodel JavaScript framework for building
user interfaces and single-page applications.

### React.js

<search-anywhere query="What is React.js?"></search-anywhere>
<br> </br>

React is a JavaScript library for building user interfaces.

React can also render on the server using Node and power mobile apps using React
Native.

### Angular

<search-anywhere query="What is Angular?"></search-anywhere>
<br> </br>


A client-side JavaScript framework which is based on MVC architecture. It
simplifies web development by offering automatic view/model synchronization.

### Rust

<search-anywhere query="What is Rust?"></search-anywhere>
<br> </br>
Systems programming language sponsored by Mozilla Research, which describes it as a "safe, concurrent, practical language," supporting functional and imperative-procedural paradigms. Systems programming language that runs blazingly fast, prevents segfaults, and guarantees thread safety.


_From [Glossary Tech](https://glossarytech.com/)_

### C 

<search-anywhere query="What is the C language?"></search-anywhere>
<br> </br>
General-purpose, high-level language that was originally designed to develop the UNIX operating system at Bell Labs. C has now become a widely used professional language because it’s easy to learn and can handle low-level activities.

_From [Glossary Tech](https://glossarytech.com/)_

### C++ 

<search-anywhere query="What is the C++ language?"></search-anywhere>
<br> </br>
An object-oriented programming language and incorporates all the features offered by C. C++ started its journey as C with classes. Gradually, it has evolved and despite the popularity of other programming languages like C# and Java, C, C++ holds its own as one of the most widely used languages for scripting.  In applications, C++ is ubiquitous.

_From [Glossary Tech](https://glossarytech.com/)_

### Docker

<search-anywhere query="What is Docker?"></search-anywhere>
<br> </br>
A software virtualization platform that allows user create a container inside your computer. Container is a small virtual computer with it’s own OS and all setup that is needed, where the user can run any software as on the usual computer..

_From [Glossary Tech](https://glossarytech.com/)_

### Shell

<search-anywhere query="What is Shell?"></search-anywhere>
<br> </br>
A software interface that enables the user to interact with the computer, it provides UI for access to an OS's services. Shells use either a command-line interface or GUI, it is an environment in which we can run our commands, programs, and shell scripts.


_From [Glossary Tech](https://glossarytech.com/)_

### Bash

<search-anywhere query="What is Bash?"></search-anywhere>
<br> </br>
Bourne-Again SHell is the most popular command shell in unix-like systems, especially in GNU / Linux. BASH is not only a command shell, it is also an excellent scripting programming language.


_From [Glossary Tech](https://glossarytech.com/)_

### Shell script

<search-anywhere query="What is Shell script?"></search-anywhere>
<br> </br>
A command-line interpreter, contains a sequence of commands for a Unix-based operating system, usually created for command sequences for which a user has a repeated need


_From [Glossary Tech](https://glossarytech.com/)_

### Open Source

<search-anywhere query="What is Open Source?"></search-anywhere>
<br> </br>
A software development model, that is decentralized and encourages open collaboration. Any program whose source code is freely available to the public. Unlike commercial software, they can be used and modified by anyone and are often developed as a community rather than by a single organization. 

_From [Glossary Tech](https://glossarytech.com/)_

### Linux

<search-anywhere query="What is Linux?"></search-anywhere>
<br> </br>
Unix-like operating system based on the Linux kernel, including one or another set of utilities and GNU project programs, and other components. Like the Linux kernel, systems based on it are generally created and distributed in accordance with the model of free and open source.

_From [Glossary Tech](https://glossarytech.com/)_

### Front End

<search-anywhere query="What is Front End?"></search-anywhere>
<br> </br>
Front-end is a term that involves the building of webpages and user interfaces for web-applications. It implements the structure, design, behavior, and animation of everything you see on the screen when you open up websites, web applications, or mobile apps. The core 3 technologies that all modern front-end web developers work to master are HTML5, CSS, and JavaScript.  

_From [Glossary Tech](https://glossarytech.com/)_

### Back End

<search-anywhere query="What is Back End?"></search-anywhere>
<br> </br>
Or the 'server-side' of website development. Basically it is the programming that users don’t see in the browser, but what also powers the website. Back-end facilitates communication between the browser and server, provides smooth functionality akin to a desktop application.

_From [Glossary Tech](https://glossarytech.com/)_

### DevOps

<search-anywhere query="What is DevOps?"></search-anywhere>
<br> </br>
Development + Operations. Software development method that emphasizes communication, collaboration, integration and automation. The method acknowledges the interdependence of software development, QA, and IT operations, and aims to help an organization rapidly produce software products and services and to improve operations performance.

_From [Glossary Tech](https://glossarytech.com/)_

### Semantic Markup

<search-anywhere query="Semantic Markup"></search-anywhere>
<br> </br>

Semantics refers to the correct interpretation of the meaning of a word or
sentence.

To use a word semantically is to use it in a way that is properly aligned with
the meaning of the word. When we misuse a word we are not using it semantically.

Many HTML tags have semantic meaning. That is, the element itself conveys some
information about the type of content contained between the opening and closing
tags.

[Read more about Semantic Markup in HTML](https://html.com/semantic-markup/)

### Bayesian Statistics


<search-anywhere query="Bayesian Statistics"></search-anywhere>
<br> </br>


Bayesian statistics is a theory in the field of statistics based on the Bayesian
interpretation of probability where probability expresses a degree of belief in
an event.

_From [Glossary Tech](https://glossarytech.com/)_

### Big Data

<search-anywhere query="Big Data"></search-anywhere>
<br> </br>


A term that describes the large volume of data – both structured and
unstructured – that inundates a business on a day-to-day basis. Can be analyzed
for insights that lead to better decisions and strategic business moves.

_From [Glossary Tech](https://glossarytech.com/)_

### Computer Vision

<search-anywhere query="Computer Vision"></search-anywhere>
<br> </br>

Computer vision is a field of computer science that works on enabling computers
to see, identify, and process images in the same way that human vision does, and
then provide appropriate output.

_From [Glossary Tech](https://glossarytech.com/)_

### Convolutional Neural Network

<search-anywhere query="Convolutional Neural Network"></search-anywhere>
<br> </br>

Convolutional neural network (CNN, or ConvNet) is a class of deep neural
networks, most commonly applied to analyzing visual imagery.

They have applications in image and video recognition, recommender systems,
image classification, medical image analysis, and natural language processing.

_From [Glossary Tech](https://glossarytech.com/)_

### Data Structure

<search-anywhere query="Data Structure"></search-anywhere>
<br> </br>

A specialized format for organizing and storing data. Serves as the basis for
abstract data types.

General data structure types include the array, the file, the record, the table,
the tree, and so on.

_From [Glossary Tech](https://glossarytech.com/)_

### Data Visualization

<search-anywhere query="Data Visualization"></search-anywhere>
<br> </br>

Data visualization is the graphical representation of information and data.

By using visual elements like charts, graphs, and maps, data visualization tools
provide an accessible way to see and understand trends, outliers, and patterns
in data.

_From [Glossary Tech](https://glossarytech.com/)_

### Hadoop

<search-anywhere query="What is Hadoop?"></search-anywhere>
<br> </br>

An open-source software framework that is used for distributed storage and
processing of big data sets across clusters of computers using simple
programming models.

_From [Glossary Tech](https://glossarytech.com/)_

### Neural Networks

<search-anywhere query="Neural Networks"></search-anywhere>
<br> </br>

Neural networks(NN) are a set of algorithms, modeled loosely after the human
brain, that are designed to recognize patterns. They interpret sensory data
through a kind of machine perception, labeling or clustering raw input.

_From [Glossary Tech](https://glossarytech.com/)_

### Pattern Recognition

<search-anywhere query="Pattern Recognition"></search-anywhere>
<br> </br>

Pattern recognition is the process of recognizing patterns by using machine
learning algorithm.

Pattern recognition can be defined as the classification of data based on
knowledge already gained or on statistical information extracted from patterns
and/or their representation.

_From [Glossary Tech](https://glossarytech.com/)_

### Random Forest

<search-anywhere query="Random Forest"></search-anywhere>
<br> </br>

Pattern recognition is the process of recognizing patterns by using machine
learning algorithm.

Pattern recognition can be defined as the classification of data based on
knowledge already gained or on statistical information extracted from patterns
and/or their representation.

_From [Glossary Tech](https://glossarytech.com/)_

### Statistical Modeling

<search-anywhere query="Statistical Modeling"></search-anywhere>
<br> </br>

Statistical modeling is a simplified, mathematically-formalized way to
approximate reality (i.e. what generates your data) and optionally to make
predictions from this approximation.

The statistical model is the mathematical equation that is used.

_From [Glossary Tech](https://glossarytech.com/)_

### Time Series Analysis

<search-anywhere query="Time Series Analysis"></search-anywhere>
<br> </br>


Time series analysis is a statistical technique that deals with time series
data, or trend analysis.

There are two main goals of time series analysis:

- Identifying the nature of the phenomenon represented by the sequence of
  observations
- Forecasting (predicting future values of the time series variable).

_From [Glossary Tech](https://glossarytech.com/)_

### Blockchain

<search-anywhere query="Blockchain"></search-anywhere>
<br> </br>

### Altcoin

<search-anywhere query="Altcoin"></search-anywhere>
<br> </br>

Altcoin is an abbreviation of “Bitcoin alternative”.

Currently, the majority of altcoins are forks of Bitcoin with usually minor
changes to the proof of work (POW) algorithm of the Bitcoin blockchain.

The most prominent altcoin is Litecoin. Litecoin introduces changes to the
original Bitcoin protocol such as decreased block generation time, increased
maximum number of coins and different hashing algorithm.

_From [Blockchain Hub](https://blockchainhub.net/blockchain-glossary/)_

### ASIC

<search-anywhere query="ASIC"></search-anywhere>
<br> </br>

An “Application Specific Integrated Circuit” is a silicon chip specifically
designed to do a single task.

In the case of Bitcoin, they are designed to process SHA-256 hashing problems to
mine new bitcoins. ASICs are considered to be much more efficient than
conventional hardware(CPUs, GPUs).

Using a regular computer for Bitcoin mining is seen as unprofitable and only
results in higher electricity bills.

_From [Blockchain Hub](https://blockchainhub.net/blockchain-glossary/)_

### Bitcoin

<search-anywhere query="Bitcoin"></search-anywhere>
<br> </br>

Bitcoin is a decentralized digital currency without a central bank or single
administrator that can be sent from user to user on the peer-to-peer bitcoin
network without the need for intermediaries.

Bitcoin is a cryptocurrency that:

- Runs on a global peer to peer network
- Is decentralised (no single entity can control it)
- Is open source
- Bypasses middlemen or central authority with no issuer or acquirer
- Anyone with a computer or smartphone can use it

_From [Blockchain Hub](https://blockchainhub.net/blockchain-glossary/)_

### Ethereum

<search-anywhere query="Ethereum"></search-anywhere>
<br> </br>

Ethereum is an open software platform based on blockchain technology that
enables developers to write smart contracts and build and deploy decentralized
applications(Dapps).

The native token of the blockchain is called Ether which is used to pay for
transaction fees, miner rewards and other services on the network.

The main innovation of Ethereum is the Ethereum Virtual Machine (EVM) which runs
on the Ethereum network and enables anyone to run any application. The EVM makes
the process of developing blockchain applications much easier.

Before the emergence of Ethereum, developers had to develop a dedicated
blockchain for each application they wanted to create. This process is
time-consuming and resource-intensive.

As opposed to Bitcoin, its scripting language is Turing-complete and
full-featured, expanding the kinds of smart contracts that it can support. The
Ethereum project wants to “decentralize the web” by introducing four components
as part of its roadmap:

- Static content publication
- Dynamic messages
- Trustless transactions
- An integrated user-interface

_From [Blockchain Hub](https://blockchainhub.net/blockchain-glossary/)_

### Unix

<search-anywhere query="What is Unix?"></search-anywhere>
<br> </br>
Family of portable, multitasking and multi-user operating systems. The ideas behind UNIX had a huge impact on the development of computer operating systems. Currently, UNIX-systems are recognized as one of the most historically important OS.


_From [Glossary Tech](https://glossarytech.com/)_
